FROM mysql:5.7

MAINTAINER Adam Sanchez <a.sanchez75@gmail.com>

ADD docker.cnf /etc/mysql/conf.d/docker.cnf

RUN touch /var/log/mysql/slow_queries.log && \
    chmod 660 /var/log/mysql/slow_queries.log && \
    chown mysql:mysql /var/log/mysql/slow_queries.log

RUN touch /var/log/mysql/mysql_error.log && \
    chmod 660 /var/log/mysql/mysql_error.log && \
    chown mysql:mysql /var/log/mysql/mysql_error.log
