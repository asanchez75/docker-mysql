### Command to visualize mysql entries log in journalctl

```
(base) root@sides /h/a/d/docker-mysql# journalctl -f CONTAINER_NAME=mysql
-- Logs begin at Fri 2021-08-13 16:08:32 CEST. --
Dec 19 10:04:32 sides 588c89001c33[16434]: 2021-12-19 09:04:32+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.36-1debian10 started.
Dec 19 10:13:03 sides 6f96be9b3f67[16434]: 2021-12-19 09:13:03+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.36-1debian10 started.
Dec 19 10:13:03 sides 6f96be9b3f67[16434]: 2021-12-19 09:13:03+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
Dec 19 10:13:03 sides 6f96be9b3f67[16434]: 2021-12-19 09:13:03+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.36-1debian10 started.
Dec 19 12:46:09 sides 23187adea79c[16434]: 2021-12-19 11:46:09+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.36-1debian10 started.
Dec 19 12:46:09 sides 23187adea79c[16434]: 2021-12-19 11:46:09+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
Dec 19 12:46:09 sides 23187adea79c[16434]: 2021-12-19 11:46:09+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.36-1debian10 started.
Dec 19 12:48:54 sides 1d4e2855cdc5[16434]: 2021-12-19 11:48:54+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.36-1debian10 started.
Dec 19 12:48:54 sides 1d4e2855cdc5[16434]: 2021-12-19 11:48:54+00:00 [Note] [Entrypoint]: Switching to dedicated user 'mysql'
Dec 19 12:48:54 sides 1d4e2855cdc5[16434]: 2021-12-19 11:48:54+00:00 [Note] [Entrypoint]: Entrypoint script for MySQL Server 5.7.36-1debian10 started.
```


### Adding logrotate file 

**/var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log** is the path to the mysql slow log located at the docker volume 

```
(base) root@sides /h/a/d/docker-mysql# cat /etc/logrotate.d/mysql-slow-queries
/var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log {
   daily
   rotate 7
   compress
   missingok
   notifempty
   sharedscripts
   postrotate
     docker exec -it mysql bash -c "mysql -uroot -p123456 -e 'select @@global.long_query_time into @lqt_save; set global long_query_time=2000; select sleep(2); FLUSH LOGS; select sleep(2); set global long_query_time=@lqt_save;'"
   endscript
}

```

### Testing

It uses **mysqlslap** for workload testing and to generate entries in the mysql slow log

```
docker exec -it mysql mysqlslap --user=root --password --host=localhost --concurrency=500  --auto-generate-sql --verbose
```

### Rotating log files

```
logrotate -vf /etc/logrotate.d/mysql-slow-queries 

reading config file /etc/logrotate.d/mysql-slow-queries
Reading state from file: /var/lib/logrotate/status
Allocating hash table for state file, size 64 entries
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state
Creating new state

Handling 1 logs

rotating pattern: /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log  forced from command line (7 rotations)
empty log files are not rotated, old logs are removed
considering log /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log
  Now: 2021-12-19 12:59
  Last rotated at 2021-12-19 12:49
  log needs rotating
rotating log /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log, log->rotateCount is 7
dateext suffix '-20211219'
glob pattern '-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
renaming /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.7.gz to /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.8.gz (rotatecount 7, logstart 1, i 7),
old log /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.7.gz does not exist
renaming /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.6.gz to /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.7.gz (rotatecount 7, logstart 1, i 6),
old log /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.6.gz does not exist
renaming /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.5.gz to /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.6.gz (rotatecount 7, logstart 1, i 5),
old log /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.5.gz does not exist
renaming /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.4.gz to /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.5.gz (rotatecount 7, logstart 1, i 4),
old log /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.4.gz does not exist
renaming /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.3.gz to /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.4.gz (rotatecount 7, logstart 1, i 3),
old log /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.3.gz does not exist
renaming /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.2.gz to /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.3.gz (rotatecount 7, logstart 1, i 2),
old log /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.2.gz does not exist
renaming /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.1.gz to /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.2.gz (rotatecount 7, logstart 1, i 1),
renaming /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.0.gz to /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.1.gz (rotatecount 7, logstart 1, i 0),
old log /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.0.gz does not exist
log /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.8.gz doesn't exist -- won't try to dispose of it
renaming /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log to /var/lib/docker/volumes/docker-mysql_mysql_log/_data/slow_queries.log.1
running postrotate script
mysql: [Warning] Using a password on the command line interface can be insecure.
+----------+
| sleep(2) |
+----------+
|        0 |
+----------+
+----------+
| sleep(2) |
+----------+
|        0 |
+----------+
compressing log with: /bin/gzip

```


